<font size ="5"> v1.0.1 - 20230601</font>
<hr style="1px solid">

<ul>
<li>
[KMIAILABOR-669] Remove addition <a href="https://gitlab.com/taquanghuy/gitexercise/-/merge_requests/8"> PR#8 </a>
</li>
</ul>

<font size ="5"> v1.0.0 - 20230601</font>
<hr style="1px solid">

<ul>
<li>
[KMIAILABOR-664] Implement addition <a href="https://gitlab.com/taquanghuy/gitexercise/-/merge_requests/1"> PR#1 </a>
</li>
<li>
[KMIAILABOR-666] Implement multiplication <a href="https://gitlab.com/taquanghuy/gitexercise/-/merge_requests/2"> PR#2 </a>
</li>
<li>
[KMIAILABOR-665] Implement subtraction <a href="https://gitlab.com/taquanghuy/gitexercise/-/merge_requests/3"> PR#3 </a>
</li>
<li>
[KMIAILABOR-667] Implement division <a href="https://gitlab.com/taquanghuy/gitexercise/-/merge_requests/4"> PR#4 </a>
</li>
</ul>
